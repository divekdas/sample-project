
// Banner Slider
$('.owl-banner').owlCarousel({
    items:1,
    loop:true,
    margin:0,
    nav:false,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
})

// Trusted by
$('.owl-trustedby').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    margin:24,
    autoplay:true,
    autoplayHoverPause:true,
    autoHeight: true,
    autoHeightClass: 'owl-height',
    dots:false,
    responsive:{
        0:{
            items:2
        },
        900:{
            items:4
        },
        1200:{
            items:7
        }
    }
})

// Certification
$('.owl-certification').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    margin:24,
    autoplay:false,
    autoplayHoverPause:true,
    autoHeight: true,
    autoHeightClass: 'owl-height',
    dots:false,
    responsive:{
        0:{
            items:1
        },
        900:{
            items:2
        }
    }
})

// Certification
$('.owl-testimonial').owlCarousel({
    loop:true,
    center:true,
    margin:0,
    nav:false,
    margin:50,
    autoplay:false,
    dots:true,
    autoWidth:true,
    responsive:{
        0:{
            items:1
        },
        900:{
            items:1
        }
    }
})